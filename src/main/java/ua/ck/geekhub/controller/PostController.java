package ua.ck.geekhub.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ck.geekhub.service.PostService;
import ua.ck.geekhub.service.UserService;

/**
 * Created by Виктор on 13.04.14.
 */
@Controller

public class PostController {
    @Autowired
    PostService postService;
    @Autowired
    UserService userService;

    @RequestMapping(value = "/createPost", method = RequestMethod.GET)

    public String post() {
        // model.addAttribute("posts", postService.getPosts());
        return "forms/wallPostForm";
    }

    @RequestMapping(value = "/createPost", method = RequestMethod.POST)

    public String createPost(
            ModelMap modelMap,

            @RequestParam(value = "title") String title,
            @RequestParam(value = "createdBy") int createdBy,
            @RequestParam(value = "attachedTo") int attachedTo
    ) {
        postService.createPost(title, attachedTo, createdBy);
        modelMap.addAttribute("posts", postService.getPostsByAttachTo(attachedTo));
              return "forms/postWallSystem";
    }


}
