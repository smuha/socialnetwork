package ua.ck.geekhub.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.ck.geekhub.model.Message;
import ua.ck.geekhub.model.User;
import ua.ck.geekhub.service.ConversationService;
import ua.ck.geekhub.service.IgnoreService;
import ua.ck.geekhub.service.MessageService;
import ua.ck.geekhub.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created by Mukha Viacheslav on 04.04.14.
 */

@Controller
public class ConversationController {

    @Autowired
    UserService userService;

    @Autowired
    ConversationService conversationService;

    @Autowired
    MessageService messageService;

    @Autowired
    IgnoreService ignoreService;

    @RequestMapping(value = "/conversations", method = RequestMethod.GET)
    public String conversations(ModelMap modelMap, HttpSession session) {
        modelMap.addAttribute("conversations", conversationService
                .getUserConversations(((User) session.getAttribute("loggedUser")).getId()));

        return "conversations";
    }

    @RequestMapping(value = "/sendMessage", method = RequestMethod.GET)
    public String sendMessageGet(
            ModelMap modelMap,
            @RequestParam(value = "receiverId") int receiverId
    ) {
        modelMap.addAttribute("messageReceiver", userService.getUser(receiverId));
        return "writeMessage";
    }

    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    public String sendMessagePost(
            @RequestParam(value = "receiverId") int receiverId,
            @RequestParam(value = "message") String message,
            HttpSession session
    ) {
        User loggedUser = (User) session.getAttribute("loggedUser");
        if (!conversationService.isConversation(loggedUser.getId(), receiverId)) {
            conversationService.createConversation(loggedUser.getId(), receiverId);
        }

        messageService.saveMessage(loggedUser.getId(), receiverId, message);

        return "redirect:profile?id=" + receiverId;
    }

    @RequestMapping(value = "/sendAjaxMessage", method = RequestMethod.POST)
    public
    @ResponseBody
    String sendAjaxMessage(
            @RequestParam(value = "receiverId") int receiverId,
            @RequestParam(value = "message") String message,
            HttpSession session
    ) {
        if (ignoreService.isIgnoredUser((User) session.getAttribute("loggedUser"), userService.getUser(receiverId))) {
            return "ignored";
        } else {
            User loggedUser = (User) session.getAttribute("loggedUser");
            if (!conversationService.isConversation(loggedUser.getId(), receiverId)) {
                conversationService.createConversation(loggedUser.getId(), receiverId);
            }

            messageService.saveMessage(loggedUser.getId(), receiverId, message);

            return "success";
        }
    }

    @RequestMapping(value = "chat", method = RequestMethod.GET)
    private String chat(
            ModelMap modelMap,
            HttpSession session,
            @RequestParam(value = "conversationId") int conversationId
    ) {
        modelMap.addAttribute("messages", messageService.getMessages(conversationId));
        User loggedUser = (User) session.getAttribute("loggedUser");
        User userOne = conversationService.getConversationById(conversationId).getUserOne();
        if (loggedUser.getId() == userOne.getId()) {
            modelMap.addAttribute("messageReceiver", conversationService.getConversationById(conversationId).getUserTwo());
        } else {
            modelMap.addAttribute("messageReceiver", userOne);
        }
        messageService.messagesLooked(conversationId, loggedUser.getId());
        return "chat";
    }

    @RequestMapping(value = "getMessages", method = RequestMethod.POST)
    private
    @ResponseBody
    List<Message> getMessages(
            @RequestParam(value = "conversationId") int conversationId,
            HttpSession session
    ) throws IOException {
        List<Message> messages = messageService.getMessages(conversationId);
        for (Message message : messages) {
            message.setCreatedAtStr(message.getCreatedAt().toString());
            if ((message.getAuthor().getId() != ((User) session.getAttribute("loggedUser")).getId())
                    && (message.getLooked() == 0)) {
                message.setLooked(new Byte("1"));
                messageService.updateMessage(message);
            }
        }
        return messages;
    }
}
