package ua.ck.geekhub.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ck.geekhub.service.PostCommentService;
import ua.ck.geekhub.service.PostService;
import ua.ck.geekhub.service.UserService;

/**
 * Created by Виктор on 15.04.14.
 */
@Controller
public class PostCommentController {
    @Autowired
    PostService postService;
    @Autowired
    UserService userService;
    @Autowired
    PostCommentService postCommentService;

    @RequestMapping(value = "/createComment", method = RequestMethod.GET)
    public String postComment(ModelMap model) {
        model.addAttribute("postComments", postCommentService.getPostComments());
        return "profile";
    }

    @RequestMapping(value = "/createComment", method = RequestMethod.POST)
    public String createPostComment(
            ModelMap modelMap,
            @RequestParam String comment,
            @RequestParam Integer user,
            @RequestParam Integer post


    ) {
        postCommentService.createPostComment(comment, user, post);
        modelMap.addAttribute("postComments", postCommentService.getPostComments());
        //System.out.println(request.getRequestURL());
        return "redirect:profile?id=" + user;
    }
}
