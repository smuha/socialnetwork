package ua.ck.geekhub.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ck.geekhub.model.User;
import ua.ck.geekhub.service.CityService;
import ua.ck.geekhub.service.UserService;

import javax.servlet.http.HttpSession;

/**
 * Created by Mukha Viacheslav on 30.03.14.
 */

@Controller
public class IdentificationController {
    @Autowired
    UserService userService;

    @Autowired
    CityService cityService;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration() {
        return "registration";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(
            ModelMap modelMap,
            HttpSession session,
            @RequestParam(value = "firstName") String firstName,
            @RequestParam(value = "lastName") String lastName,
            @RequestParam(value = "email") String email,
            @RequestParam(value = "phone") String phone,
            @RequestParam(value = "password") String password
    ) {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPhone(phone);
        user.setPassword(password);

        userService.saveUser(user);

        modelMap.addAttribute("id", user.getId());
        session.setAttribute("entered", true);
        session.setAttribute("loggedUser", user);
        return "redirect:profile";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginGet() {
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginPost(
            HttpSession session,
            @RequestParam(value = "email") String email,
            @RequestParam(value = "password") String password
    ) {
        User user = userService.getLoggedUser(email, password);
        if (user != null) {
            session.setAttribute("loggedUser", user);
            session.setAttribute("entered", true);
            return "redirect:profile?id=" + user.getId();
        } else {
            System.out.println("ERROR! WRONG OR DUPLICATE EMAIL!!!");
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpSession session) {
        session.setAttribute("entered", false);
        return "redirect:/";
    }
}