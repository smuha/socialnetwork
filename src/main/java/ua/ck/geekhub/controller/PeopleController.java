package ua.ck.geekhub.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import sun.print.resources.serviceui;
import sun.print.resources.serviceui_sv;
import ua.ck.geekhub.model.User;
import ua.ck.geekhub.service.FriendService;
import ua.ck.geekhub.service.IgnoreService;
import ua.ck.geekhub.service.UserService;

import javax.servlet.http.HttpSession;

/**
 * Created by Mukha Viacheslav on 08.04.14.
 */
@Controller
public class PeopleController {

    @Autowired
    UserService userService;

    @Autowired
    IgnoreService ignoreService;

    @Autowired
    FriendService friendService;

    @RequestMapping(value = "/people", method = RequestMethod.GET)
    public String peoples(
            ModelMap modelMap,
            HttpSession session
    ) {
        modelMap.addAttribute("people", userService.getUsers());
        modelMap.addAttribute("friends", friendService.getUserFriends(((User) session.getAttribute("loggedUser")).getId()));
        modelMap.addAttribute("ignoredIdUsers", ignoreService.getIgnoredInUsers((User) session.getAttribute("loggedUser")));
        return "people";
    }

}
