package ua.ck.geekhub.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import sun.print.resources.serviceui;
import ua.ck.geekhub.model.User;
import ua.ck.geekhub.service.FriendService;
import ua.ck.geekhub.service.IgnoreService;
import ua.ck.geekhub.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Muhka Viacheslav on 04.04.14.
 */

@Controller
public class FriendController {

    @Autowired
    FriendService friendService;

    @Autowired
    UserService userService;

    @Autowired
    IgnoreService ignoreService;

    @RequestMapping(value = "/friends", method = RequestMethod.GET)
    public String friends(
            ModelMap modelMap,
            @RequestParam(value = "id") int id
    ) {
        modelMap.addAttribute("friends", friendService.getUserFriends(id));
        modelMap.addAttribute("user", userService.getUser(id));
        return "friends";
    }

    @RequestMapping(value = "/notConfirmedFriends", method = RequestMethod.GET)
    public String notConfirmedFriends(
            ModelMap modelMap,
            HttpSession session
    ) {
        modelMap.addAttribute("notConfirmedFriends", friendService.getNotConfirmedFriends(((User) session.getAttribute("loggedUser")).getId()));
        return "friends";
    }

    @RequestMapping(value = "/addFriend", method = RequestMethod.POST)
    public String addFriend(
            @RequestParam(value = "id") int friendId,
            HttpSession session
    ) {
        if (ignoreService.isIgnoredUser(userService.getUser(friendId), (User) session.getAttribute("loggedUser"))) {
            return "error/removeUserFromIgnoreList";
        } else {
            User loggedUser = (User) session.getAttribute("loggedUser");
            friendService.sendFriendRequest(loggedUser, userService.getUser(friendId));
            return "redirect:profile?id=" + friendId;
        }
    }

    @RequestMapping(value = "/removeFriend", method = RequestMethod.POST)
    public String removeFriend(
            @RequestParam(value = "id") int friendId,
            HttpSession session,
            HttpServletRequest request
    ) {
        User loggedUser = (User) session.getAttribute("loggedUser");
        friendService.removeFriend(loggedUser, userService.getUser(friendId));
        return "redirect:" + request.getHeader("Referer");
    }
}
