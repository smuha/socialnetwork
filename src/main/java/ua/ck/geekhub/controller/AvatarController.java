package ua.ck.geekhub.controller;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import ua.ck.geekhub.model.User;
import ua.ck.geekhub.service.UserService;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mukha Viacheslav on 06.04.14.
 */

@Controller
public class AvatarController {

    @Autowired
    UserService userService;

    @RequestMapping(value="/uploadAvatar", method= RequestMethod.GET)
    public String provideUploadInfo() {
        return "edit/uploadAvatar";
    }

    @RequestMapping(value="/uploadAvatar", method=RequestMethod.POST)
    public @ResponseBody Map handleFileUpload(
            @RequestParam("file") MultipartFile uploadedFile,
            HttpSession session
    ) throws IOException {
        HashMap<String, String> result = new HashMap();
        User loggedUser = (User) session.getAttribute("loggedUser");
        result.put("oldPathToAvatar", (loggedUser).getPathToCurrentAvatar());
        ServletContext context = session.getServletContext();
        if (!uploadedFile.isEmpty()) {
            try {

                File realFilePass = new File(context.getRealPath("resourses/avatars/")
                        + File.separator + loggedUser.getEmail().hashCode());
                System.out.println(loggedUser.getEmail().hashCode());
                if (!realFilePass.exists()) {
                    realFilePass.mkdir();
                }

                File realFile = new File(realFilePass.getPath() + File.separator
                        + (new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date())) + "."
                        + FilenameUtils.getExtension(uploadedFile.getOriginalFilename()));
                uploadedFile.transferTo(realFile);

                loggedUser.setPathToCurrentAvatar(loggedUser.getEmail().hashCode() + File.separator + realFile.getName());
                userService.updateUser(loggedUser);

                result.put("state", "success");
                result.put("newPathToAvatar", loggedUser.getPathToCurrentAvatar());
                result.put("message", "You successfully uploaded new avatar!");
                return result;
            } catch (Exception e) {
                result.put("state", "error");
                result.put("message", "You failed to upload! ");
                return result;
            }
        } else {
            result.put("state", "error");
            result.put("message", "You failed to upload because the file was empty!");
            return result;
        }
    }

}
