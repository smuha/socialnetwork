package ua.ck.geekhub.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.ck.geekhub.model.City;
import ua.ck.geekhub.model.Country;
import ua.ck.geekhub.model.Post;
import ua.ck.geekhub.model.User;
import ua.ck.geekhub.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * Created by Mukha Viacheslav on 31.03.14.
 */

@Controller
public class ProfileController {

    @Autowired
    UserService userService;

    @Autowired
    CityService cityService;

    @Autowired
    CountryService countryService;

    @Autowired
    FriendService friendService;

    @Autowired
    IgnoreService ignoreService;

    @Autowired
    PostService postService;

    @Autowired
    PostCommentService postCommentService;

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profileGet(
            ModelMap model,
            @RequestParam(value = "id") int id,
            HttpSession session
    ) {
        User user = userService.getUser(id);
        Post post = postService.getPost(id);
        //  PostComment postComment = postCommentService.getPostComment(id);
        model.addAttribute("user", user);
        model.addAttribute("posts", postService.getPostsByAttachTo(user.getId()));
        // model.addAttribute("postComments", postCommentService.getPostComments());
        if (ignoreService.isIgnoredUser((User) session.getAttribute("loggedUser"), userService.getUser(id))) {
            return "error/accessToProfileDenied";
        } else {
            model.addAttribute("isFriendToCurrentUser",
                    friendService.isFriends((User) session.getAttribute("loggedUser"), userService.getUser(id)));
            return "profile";
        }
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editUserData(ModelMap model, HttpSession session) {
        model.addAttribute("loggedUser", userService.getUser(((User) session.getAttribute("loggedUser")).getId()));
        model.addAttribute("countries", countryService.getCountries());
        if (((User) session.getAttribute("loggedUser")).getCountry() == null) {
            model.addAttribute("cities", null);
        } else {
            model.addAttribute("cities", cityService.getCitiesByCountryId(((User) session.getAttribute("loggedUser")).getCountry().getId()));
        }
        return "edit/editUserData";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateUserData(
            ModelMap model,
            HttpServletRequest request,
            HttpSession session,
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String middleName,
            @RequestParam String phone,
            @RequestParam int countryId,
            @RequestParam int cityId,
            @RequestParam("birthday") @DateTimeFormat(pattern = "yyyy-MM-dd") Date birthday
    ) {
        User user = (User) session.getAttribute("loggedUser");
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setPhone(phone);

        if (birthday == null) {
            user.setBirthday(null);
        } else {
            user.setBirthday(new java.sql.Date(birthday.getTime()));
        }

        if (!request.getParameter("countryId").equals(Country.COUNTRY_NOT_PRESENT)) {
            user.setCountry(countryService.getCountry(countryId));
        } else {
            user.setCountry(null);
        }

        if (!request.getParameter("cityId").equals(Country.COUNTRY_NOT_PRESENT)
                && !request.getParameter("countryId").equals(Country.COUNTRY_NOT_PRESENT)) {
            user.setCity(cityService.getCity(cityId));
        } else {
            user.setCity(null);
        }

        userService.updateUser(user);
        session.setAttribute("loggedUser", user);
        model.addAttribute("user", user);
        return "redirect:profile?id=" + user.getId();
    }

    @RequestMapping(value = "/updateCities", method = RequestMethod.POST)
    public
    @ResponseBody
    List<City> updateCities(@RequestParam(value = "countryId") int countryId) {
        return cityService.getCitiesByCountryId(countryId);
    }
}