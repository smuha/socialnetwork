package ua.ck.geekhub.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ck.geekhub.model.User;
import ua.ck.geekhub.service.IgnoreService;
import ua.ck.geekhub.service.UserService;

import javax.servlet.http.HttpSession;

/**
 * Created by Mukha Viacheslav on 09.04.14.
 */

@Controller
public class IgnoreController {

    @Autowired
    IgnoreService ignoreService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/ignoreUser", method = RequestMethod.GET)
    public String ignoreUser(
            @RequestParam(value = "userId") int userId,
            HttpSession session
    ) {
        ignoreService.ignoreUser((User) session.getAttribute("loggedUser"), userService.getUser(userId));
        return "redirect:ignoreList";
    }

    @RequestMapping(value = "/ignoreList", method = RequestMethod.GET)
    public String ignoreList(
            ModelMap modelMap,
            HttpSession session
    ) {
        modelMap.addAttribute("ignoredUsers", ignoreService.getIgnoreUsers((User) session.getAttribute("loggedUser")));
        modelMap.addAttribute("people", userService.getUsers());
        return "ignoreList";
    }

    @RequestMapping(value = "/removeFromIgnoreList", method = RequestMethod.GET)
    public String removeFromIgnoreList(
            @RequestParam(value = "id") int id,
            HttpSession session
    ) {
        ignoreService.removeFromIgnoreList((User) session.getAttribute("loggedUser"), userService.getUser(id));
        return "redirect:ignoreList";
    }
}
