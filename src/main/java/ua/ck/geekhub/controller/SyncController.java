package ua.ck.geekhub.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.ck.geekhub.model.Message;
import ua.ck.geekhub.model.User;
import ua.ck.geekhub.service.FriendService;
import ua.ck.geekhub.service.MessageService;

import javax.servlet.http.HttpSession;

/**
 * Created by Mukha Viacheslav on 07.04.14.
 */

@Controller
public class SyncController {

    @Autowired
    MessageService messageService;

    @Autowired
    FriendService friendService;

    @RequestMapping(value = "/checkNewFriends", method = RequestMethod.POST)
    public @ResponseBody int checkNewFriends(HttpSession session) {
        System.out.println(friendService.getAmountOfNewFriends((User) session.getAttribute("loggedUser")));
        return friendService.getAmountOfNewFriends((User) session.getAttribute("loggedUser"));
    }

    @RequestMapping(value = "/checkNewMessages", method = RequestMethod.POST)
    public @ResponseBody String checkNewMessages(HttpSession session) {

        return "0";
    }
}
