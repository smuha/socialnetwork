package ua.ck.geekhub.model;

import javax.persistence.*;

/**
 * Created by Mukha Viacheslav on 21.03.14.
 */
@Entity
@Table(name = "USERS_SETTINGS")
public class UserSettings {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    @Column(name = "POST_COMMENT_ACCESS")
    private int postCommentAccess;

    @Column(name = "WRITE_IF_NOT_FRIEND")
    private int writeIfNotFriend;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPostCommentAccess() {
        return postCommentAccess;
    }

    public void setPostCommentAccess(int postCommentAccess) {
        this.postCommentAccess = postCommentAccess;
    }

    public int getWriteIfNotFriend() {
        return writeIfNotFriend;
    }

    public void setWriteIfNotFriend(int writeIfNotFriend) {
        this.writeIfNotFriend = writeIfNotFriend;
    }
}
