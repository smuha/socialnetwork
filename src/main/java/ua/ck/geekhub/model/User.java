package ua.ck.geekhub.model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Mukha Viacheslav on 20.03.14.
 */

@Entity
@Table(name = "USERS")
public class User {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private int id;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "LAST_NAME")
	private String lastName;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

	@Column(name = "EMAIL")
	private String email;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "BIRTHDAY")
    private Date birthday;

    @Column(name = "CREATED_AT")
    private Timestamp createdAt;

    @Column(name = "PHONE")
    private String phone;

    @ManyToOne
    @JoinColumn(name = "COUNTRY_ID")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "CITY_ID")
    private City city;

    @Column(name = "LAST_SEEN_AT")
    private Timestamp lastSeenAt;

    @Column(name = "SCHOOL_ID")
    private int schoolId;

    @Column(name = "WENT_TO_SCHOOL_AT")
    private Date wentToSchoolAt;

    @Column(name = "FINISHED_SCHOOL_AT")
    private Date finishedSchoolAt;

    @Column(name = "UNIVERSITY_ID")
    private int universityId;

    @Column(name = "WENT_TO_UNIVERSITY_AT")
    private Date wentToUniversityAt;

    @Column(name = "FINISHED_UNIVERSITY_AT")
    private Date finishedUniversityAt;

    @Column(name = "CURRENT_AVATAR_NAME")
    private String pathToCurrentAvatar;

    public String getPathToCurrentAvatar() {
        return pathToCurrentAvatar;
    }

    public void setPathToCurrentAvatar(String currentAvatarName) {
        this.pathToCurrentAvatar = currentAvatarName;
    }

    public User() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country countryId) {
        this.country = countryId;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Timestamp getLastSeenAt() {
        return lastSeenAt;
    }

    public void setLastSeenAt(Timestamp lastSeenAt) {
        this.lastSeenAt = lastSeenAt;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public Date getWentToSchoolAt() {
        return wentToSchoolAt;
    }

    public void setWentToSchoolAt(Date wentToSchoolAt) {
        this.wentToSchoolAt = wentToSchoolAt;
    }

    public Date getFinishedSchoolAt() {
        return finishedSchoolAt;
    }

    public void setFinishedSchoolAt(Date finishedSchoolAt) {
        this.finishedSchoolAt = finishedSchoolAt;
    }

    public int getUniversityId() {
        return universityId;
    }

    public void setUniversityId(int universityId) {
        this.universityId = universityId;
    }

    public Date getWentToUniversityAt() {
        return wentToUniversityAt;
    }

    public void setWentToUniversityAt(Date wentToUniversityAt) {
        this.wentToUniversityAt = wentToUniversityAt;
    }

    public Date getFinishedUniversityAt() {
        return finishedUniversityAt;
    }

    public void setFinishedUniversityAt(Date finishedUniversityAt) {
        this.finishedUniversityAt = finishedUniversityAt;
    }

//    @OneToMany
//    private List<User> friends;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
