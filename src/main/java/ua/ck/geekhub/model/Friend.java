package ua.ck.geekhub.model;

import javax.persistence.*;

/**
 * Created by Mukha Viacheslav on 21.03.14.
 */
@Entity
@Table(name = "FRIENDS")
public class Friend {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    @ManyToOne
    @JoinColumn(name = "USER_ONE_ID")
    private User userOne;

    @ManyToOne
    @JoinColumn(name = "USER_TWO_ID")
    private User userTwo;

    //this field should be deleted
    @Column(name = "IGNORE_USER_ONE")
    private int ignoreUserOne;

    //this field should be deleted
    @Column(name = "IGNORE_USER_TWO")
    private int ignoreUserTwo;

    @Column(name = "CONFIRMED")
    private int confirmed;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUserOne() {
        return userOne;
    }

    public void setUserOne(User userOne) {
        this.userOne = userOne;
    }

    public User getUserTwo() {
        return userTwo;
    }

    public void setUserTwo(User userTwo) {
        this.userTwo = userTwo;
    }

    public int getIgnoreUserOne() {
        return ignoreUserOne;
    }

    public void setIgnoreUserOne(int ignoreUserOne) {
        this.ignoreUserOne = ignoreUserOne;
    }

    public int getIgnoreUserTwo() {
        return ignoreUserTwo;
    }

    public void setIgnoreUserTwo(int ignoreUserTwo) {
        this.ignoreUserTwo = ignoreUserTwo;
    }

    public int getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }
}
