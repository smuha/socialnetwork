package ua.ck.geekhub.model;

import javax.persistence.*;

/**
 * Created by Mukha Viacheslav on 08.04.14.
 */
@Entity
@Table(name = "IGNORES")
public class Ignore {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    private User user;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "IGNORED_USER_ID")
    private User ignoredUser;

    public User getIgnoredUser() {
        return ignoredUser;
    }

    public void setIgnoredUser(User ignoreUser) {
        this.ignoredUser = ignoreUser;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User currentUser) {
        this.user = currentUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
