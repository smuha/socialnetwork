package ua.ck.geekhub.model;

import javax.persistence.*;

/**
 * Created by Admin on 21.03.14.
 */
@Entity
@Table(name = "CITIES")
public class City {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn(name = "COUNTRY_ID")
    private Country country;

    @Column(name = "NAME")
    private String name;
}
