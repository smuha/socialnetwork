package ua.ck.geekhub.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Mukha Viacheslav on 21.03.14.
 */
@Entity
@Table(name = "MESSAGES")
public class Message {
    @Id
    @GeneratedValue
    @Column
    private int id;

    @ManyToOne
    @JoinColumn(name = "CONVERSATION_ID")
    private Conversation conversation;

    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID")
    private User author;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "CREATED_AT")
    private Timestamp createdAt;

    @Column(name = "LOOKED")
    private Byte looked;

    @Transient
    private String createdAtStr;

    public String getCreatedAtStr() {
        return createdAtStr;
    }

    public void setCreatedAtStr(String createdAtStr) {
        this.createdAtStr = createdAtStr;
    }

    public Byte getLooked() {
        return looked;
    }

    public void setLooked(Byte looked) {
        this.looked = looked;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }
}
