package ua.ck.geekhub.model;

import javax.persistence.*;

/**
 * Created by Admin on 21.03.14.
 */
@Entity
@Table(name = "CONVERSATIONS")
public class Conversation {

    public static final byte NOT_LOOKED = 0;
    public static final byte LOOKED = 1;

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    @ManyToOne
    @JoinColumn(name = "USER_ONE_ID")
    private User userOne;

    @ManyToOne
    @JoinColumn(name = "User_TWO_ID")
    private User userTwo;

    @Transient
    private int amountUnreadMessages;

    public int getAmountUnreadMessages() {
        return amountUnreadMessages;
    }

    public void setAmountUnreadMessages(int amountUnreadMessages) {
        this.amountUnreadMessages = amountUnreadMessages;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUserOne() {
        return userOne;
    }

    public void setUserOne(User userOne) {
        this.userOne = userOne;
    }

    public User getUserTwo() {
        return userTwo;
    }

    public void setUserTwo(User userTwo) {
        this.userTwo = userTwo;
    }
}
