package ua.ck.geekhub.model;

import javax.persistence.*;

/**
 * Created by Mukha Viacheslav on 21.03.14.
 */
@Entity
@Table(name="COUNTRIES")
public class Country {

    public static final String COUNTRY_NOT_PRESENT = "0";

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name")

    private String name;
}
