package ua.ck.geekhub.service;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.model.Friend;
import ua.ck.geekhub.model.User;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Mukha Viacheslav on 30.03.14.
 */

@Repository
@Transactional
public class FriendService {

    @Autowired
    SessionFactory sessionFactory;

    public List<User> getUserFriends(Integer userId) {
        List<User> users = new ArrayList<>();

        List<Friend> friends = sessionFactory.getCurrentSession().createCriteria(Friend.class)
                .add(Restrictions.eq("userOne.id", userId))
                .list();
        for (Friend friend : friends) {
            users.add(friend.getUserTwo());
        }

        friends = sessionFactory.getCurrentSession().createCriteria(Friend.class)
                .add(Restrictions.eq("userTwo.id", userId))
                .list();
        for (Friend friend : friends) {
            users.add(friend.getUserOne());
        }

        return users;
    }

    public void saveFriend(Friend friend) {
        sessionFactory.getCurrentSession().save(friend);
    }

    public void updateFriend(Friend friend) {
        sessionFactory.getCurrentSession().update(friend);
    }

    public void sendFriendRequest(User userOne, User userTwo) {
        Friend friend = new Friend();
        friend.setConfirmed(0);
        friend.setIgnoreUserOne(0);
        friend.setIgnoreUserTwo(0);
        friend.setUserOne(userOne);
        friend.setUserTwo(userTwo);

        saveFriend(friend);
    }

    public List<Friend> getFriends() {
        return sessionFactory.getCurrentSession()
                .createCriteria(Friend.class)
                .list();
    }

    public boolean isFriends(User loggedUserOne, User userTwo) {
//        if (loggedUserOne.getId() == userTwo.getId()) {
//            return true;
//        }

        ArrayList<Friend> friends = new ArrayList<>(getFriends());
        HashSet<Integer> friendsId = new HashSet<>();
        for (Friend friend : friends) {
            if (friend.getUserOne().getId() == loggedUserOne.getId()) {
                friendsId.add(friend.getUserTwo().getId());
            }
            if (friend.getUserTwo().getId() == loggedUserOne.getId()) {
                friendsId.add(friend.getUserOne().getId());
            }
        }
        for (Integer friendId : friendsId) {
            if (friendId.equals(userTwo.getId())) {
                return true;
            }
        }
        friendsId.clear();

        return false;
    }

    public void removeFriend(User loggedUser, User loggedUserFriend) {
        List<Friend> friends = sessionFactory.getCurrentSession().createCriteria(Friend.class).list();
        Friend deleteFriend = null;
        for (Friend f : friends) {
            if (((f.getUserOne().getId()== loggedUser.getId()) && (f.getUserTwo().getId() == loggedUserFriend.getId()))
                    || ((f.getUserTwo().getId() == loggedUser.getId()) && (f.getUserOne().getId() == loggedUserFriend.getId()))) {
                deleteFriend = f;
            }
        }

        sessionFactory.getCurrentSession().delete(deleteFriend);
    }

    public int getAmountOfNewFriends(User user) {
        return ((Long) sessionFactory.getCurrentSession()
                .createQuery("select count(id) from Friend where userTwo.id = ? and confirmed = 0")
                .setParameter(0, user.getId())
                .uniqueResult())
                .intValue();
    }

    public List<User> getNotConfirmedFriends(Integer userId) {
        return sessionFactory.getCurrentSession()
                .createQuery("select userOne from Friend where userTwo.id = ? and confirmed = 0")
                .setParameter(0, userId)
                .list();
    }
}