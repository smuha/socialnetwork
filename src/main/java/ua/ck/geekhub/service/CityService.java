package ua.ck.geekhub.service;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.model.City;
import ua.ck.geekhub.model.User;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Mukha Viacheslav on 23.03.14.
 */
@Repository
@Transactional
public class CityService {

    @Autowired
    SessionFactory sessionFactory;

    public void saveCity(City city) {
        sessionFactory.getCurrentSession().save(city);
    }

    public void updateCity(City city) {
        sessionFactory.getCurrentSession().update(city);
    }


    public City getCity(Integer id) {
        return (City) sessionFactory.getCurrentSession().get(City.class, id);
    }

    public List<City> getCities() {
        return sessionFactory.getCurrentSession()
                .createCriteria(City.class)
                .list();
    }

    public List<City> getCitiesByCountryId(Integer countryId) {
        if (countryId == null) {
            return null;
        } else  {
            return sessionFactory.getCurrentSession()
                    .createCriteria(City.class)
                    .add(Restrictions.eq("country.id", countryId))
                    .list();
        }
    }

    public void deleteCity(City city) {
        sessionFactory.getCurrentSession().delete(city);
    }

    public void createCity(String name) {
        City city = new City();
        city.setName(name);
        saveCity(city);
    }
}
