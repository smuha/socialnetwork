package ua.ck.geekhub.service;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.model.Post;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by Виктор on 13.04.14.
 */
@Repository
@Transactional
public class PostService {
    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    UserService userService;

    public void createPost(String title, Integer attachedTo, Integer createdBy) {
        Post post = new Post();
        post.setTitle(title);
        post.setCreatedAt(new Timestamp((new Date()).getTime()));
        post.setCreatedBy(userService.getUser(createdBy));
        post.setAttachedTo(userService.getUser(attachedTo));
        sessionFactory.getCurrentSession().save(post);
    }

    public List<Post> getPosts() {
        return sessionFactory.getCurrentSession().createCriteria(Post.class).list();
    }

    public Post getPost(Integer id) {
        return (Post) sessionFactory.getCurrentSession().get(Post.class, id);
    }

    public List<Post> getPostsByAttachTo(Integer id) {
        List<Post> posts = sessionFactory.getCurrentSession()
                .createQuery("from Post where attachedTo.id = ?")
                .setParameter(0, id)
                .list();
        if (posts.isEmpty()) {
            return null;
        } else {
            return posts;
        }
    }

    public void deletePost(Post post) {
        sessionFactory.getCurrentSession().delete(post);
    }
}
