package ua.ck.geekhub.service;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.model.Conversation;
import ua.ck.geekhub.model.Message;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mukha Viacheslav on 04.04.14.
 */

@Repository
@Transactional
public class ConversationService {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    UserService userService;

    public boolean isConversation(Integer userOneId, Integer userTwoId) {
        List<Conversation> conversations = sessionFactory.getCurrentSession()
                .createCriteria(Conversation.class)
                .list();

        for (Conversation conversation : conversations) {
            if (((conversation.getUserOne().getId() == userOneId)
                    && (conversation.getUserTwo().getId() == userTwoId))
                    || ((conversation.getUserTwo().getId() == userOneId)
                    && (conversation.getUserOne().getId() == userTwoId))) {
                return true;
            }
        }

        return false;
    }

    public void createConversation(Integer userOneId, Integer userTwoId) {
        Conversation conversation = new Conversation();
        conversation.setUserOne(userService.getUser(userOneId));
        conversation.setUserTwo(userService.getUser(userTwoId));

        sessionFactory.getCurrentSession().save(conversation);
    }

    public Conversation getConversation(Integer userOneId, Integer userTwoId){
        List<Conversation> conversations = sessionFactory.getCurrentSession()
                .createQuery("from Conversation where userOne.id = " + userOneId
                        + " and userTwo.id = " + userTwoId )
                .list();
        if (!conversations.isEmpty()) {
            return conversations.get(0);
        } else {
            conversations = sessionFactory.getCurrentSession()
                    .createQuery("from Conversation where userOne.id = " + userTwoId
                            + " and userTwo.id = " + userOneId )
                    .list();
            if (!conversations.isEmpty()) {
                return conversations.get(0);
            }
            else {
                return null;
            }
        }
    }

    public List<Conversation> getUserConversations(Integer userId) {
        List<Conversation> conversations = sessionFactory.getCurrentSession()
                .createQuery("from Conversation where userOne.id = ? or userTwo.id = ?")
                .setParameter(0, userId)
                .setParameter(1, userId)
                .list();

        for (Conversation conversation : conversations) {
            int unreadMessages = ((Long) sessionFactory.getCurrentSession()
                    .createQuery("select count(id) from Message where (conversation.id = ? and author.id <> ?) and looked = ?")
                    .setParameter(0, conversation.getId())
                    .setParameter(1, userId)
                    .setParameter(2, Conversation.NOT_LOOKED)
                    .uniqueResult())
                    .intValue();
            conversation.setAmountUnreadMessages(unreadMessages);
        }

        return conversations;
    }

    public Conversation getConversationById(Integer id) {
        return (Conversation) sessionFactory.getCurrentSession()
                .get(Conversation.class, id);
    }
}
