package ua.ck.geekhub.service;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.model.Friend;
import ua.ck.geekhub.model.Ignore;
import ua.ck.geekhub.model.User;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mukha Viacheslav on 08.04.14.
 */

@Repository
@Transactional
public class IgnoreService {

    @Autowired
    SessionFactory sessionFactory;

    public void ignoreUser(User currentUser, User ignoreUser) {
        List<Ignore> ignoreList = sessionFactory.getCurrentSession()
                .createQuery("from Ignore where user.id = ? and ignoredUser.id = ?")
                .setParameter(0, currentUser.getId())
                .setParameter(1, ignoreUser.getId())
                .list();

        Ignore ignore = null;

        if (ignoreList.isEmpty()) {
            ignore = new Ignore();
            ignore.setUser(currentUser);
            ignore.setIgnoredUser(ignoreUser);
        } else {
            ignore = ignoreList.get(0);
        }
        sessionFactory.getCurrentSession().saveOrUpdate(ignore);

        //Removing each other from friends
        List<Friend> friends = sessionFactory.getCurrentSession()
                .createQuery("from Friend where ((userOne.id = ? and userTwo.id = ?) or (userTwo.id = ? and userOne.id = ?))")
                .setParameter(0, currentUser.getId())
                .setParameter(1, ignoreUser.getId())
                .setParameter(2, currentUser.getId())
                .setParameter(3, ignoreUser.getId())
                .list();
        if (!friends.isEmpty()) {
            sessionFactory.getCurrentSession().delete(friends.get(0));
        }
    }

    public List<User> getIgnoreUsers(User currentUser) {
        // I do not know why, but this code do not work.
//        return sessionFactory.getCurrentSession()
//                .createQuery("select ignoreUser from Ignore where currentUser.id = ?")
//                .setParameter(0, currentUser.getId())
//                .list();

        List<Ignore> ignores = sessionFactory.getCurrentSession()
                .createCriteria(Ignore.class)
                .add(Restrictions.like("user.id", currentUser.getId()))
                .list();

        List<User> ignoredUsers = new ArrayList<>();
        for (Ignore ignore : ignores) {
            ignoredUsers.add(ignore.getIgnoredUser());
        }
        return ignoredUsers;
    }

    /**
     * Method returns true if currentUser was ignored by user
     * @param currentUser
     * @param user
     * @return
     */
    public boolean isIgnoredUser(User currentUser, User user) {
        if (((Long) sessionFactory.getCurrentSession()
                .createQuery("select count(*) from Ignore where user.id = ? and ignoredUser.id = ?")
                .setParameter(0, user.getId())
                .setParameter(1, currentUser.getId())
                .uniqueResult())
                .intValue() == 0) {
            return false;
        } else {
            return true;
        }
    }

    public void removeFromIgnoreList(User currentUser, User user) {
        List<Ignore> ignores = sessionFactory.getCurrentSession()
                .createQuery("from Ignore where user.id = ? and ignoredUser.id = ?")
                .setParameter(0, currentUser.getId())
                .setParameter(1, user.getId())
                .list();

        sessionFactory.getCurrentSession().delete(ignores.get(0));
    }

    public List<User> getIgnoredInUsers(User user) {
        List<Ignore> ignores = sessionFactory.getCurrentSession()
                .createQuery("from Ignore where ignoredUser.id = ?")
                .setParameter(0, user.getId())
                .list();

        List<User> users = new ArrayList<>();
        for (Ignore ignore : ignores) {
            users.add(ignore.getUser());
        }
        return users;
    }
}
