package ua.ck.geekhub.service;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.model.PostComment;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by Виктор on 14.04.14.
 */
@Repository
@Transactional
public class PostCommentService {
    @Autowired
    PostService postService;
    @Autowired
    UserService userService;
    @Autowired
    SessionFactory sessionFactory;

    public void createPostComment(String comment, Integer post, Integer user) {
        PostComment postComment = new PostComment();
        postComment.setComment(comment);
        postComment.setPost(postService.getPost(post));
        postComment.setUser(userService.getUser(user));
        postComment.setCreatedAt(new Timestamp((new Date()).getTime()));

        sessionFactory.getCurrentSession().save(postComment);
    }

    public List<PostComment> getPostComments() {
        return sessionFactory.getCurrentSession()
                .createCriteria(PostComment.class)
                .list();
    }

    public List<PostComment> getPostCommentsByPost(Integer id) {

        List<PostComment> postComments = sessionFactory.getCurrentSession()
                .createQuery("From PostComment where post.id= ?")
                .setParameter(0, id)
                .list();
        if (postComments.isEmpty()) {
            return null;
        } else {
            return postComments;
        }
    }

    public PostComment getPostComment(Integer id) {
        return (PostComment) sessionFactory.getCurrentSession().get(PostComment.class, id);
    }

    public void deletePostComment(PostComment postComment) {
        sessionFactory.getCurrentSession().delete(postComment);
    }
}
