package ua.ck.geekhub.service;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.model.Conversation;
import ua.ck.geekhub.model.Message;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by Mukha Viacheslav on 04.04.14.
 */

@Repository
@Transactional
public class MessageService {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    ConversationService conversationService;

    @Autowired
    UserService userService;

    public void saveMessage(Integer authorId, Integer receiverId, String message) {
        Message mess = new Message();
        mess.setConversation(conversationService.getConversation(authorId, receiverId));
        mess.setAuthor(userService.getUser(authorId));
        mess.setCreatedAt(new Timestamp((new Date()).getTime()));
        mess.setMessage(message);
        mess.setLooked(Conversation.NOT_LOOKED);

        sessionFactory.getCurrentSession().save(mess);
    }

    public List<Message> getMessages(Integer conversationId) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Message where conversation.id = ?")
                .setParameter(0, conversationId)
                .list();
    }

    public void messagesLooked(Integer conversationId, Integer receiverId) {
        sessionFactory.getCurrentSession()
                .createQuery("update Message set looked = ? where conversation.id = ? and author.id <> ?")
                .setParameter(0, Conversation.LOOKED)
                .setParameter(1, conversationId)
                .setParameter(2, receiverId)
                .executeUpdate();
    }

    public void updateMessage(Message message) {
        sessionFactory.getCurrentSession().update(message);
    }
}
