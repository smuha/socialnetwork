package ua.ck.geekhub.service;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import sun.print.resources.serviceui;
import ua.ck.geekhub.model.Country;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Mukha Viacheslav on 02.04.14.
 */
@Repository
@Transactional
public class CountryService {

    @Autowired
    SessionFactory sessionFactory;

    public List<Country> getCountries() {
        return sessionFactory.getCurrentSession().createCriteria(Country.class).list();
    }

    public Country getCountry(Integer id) {
        return (Country) sessionFactory.getCurrentSession().get(Country.class, id);
    }
}
