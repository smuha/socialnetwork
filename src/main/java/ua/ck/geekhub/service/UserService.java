package ua.ck.geekhub.service;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class UserService {

	@Autowired
	SessionFactory sessionFactory;

	public void saveUser(User user) {
		sessionFactory.getCurrentSession().save(user);
	}

    public void updateUser(User user) {
        sessionFactory.getCurrentSession().update(user);
    }


    public User getUser(Integer id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	public List<User> getUsers() {
		return sessionFactory.getCurrentSession()
                .createCriteria(User.class)
                .list();
	}

    public void deleteUser(User user) {
        sessionFactory.getCurrentSession().delete(user);
    }

	public void createUser(String firstName, String lastName, String email) {
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		saveUser(user);
	}

    public User getUserH(Integer userId) {
        String hql = "select user from User user where user.id = :userId";
        return  (User) sessionFactory.getCurrentSession().createQuery(hql)
                .setInteger("userId", userId).uniqueResult();
    }

    public User getLoggedUser(String email, String password) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
        Criterion criterionEmail = Restrictions.eq("email", email);
        Criterion criterionPassword = Restrictions.eq("password", password);
        LogicalExpression expression = Restrictions.and(criterionEmail, criterionPassword);
        criteria.add(expression);
        List users = criteria.list();
        if (users.size() == 1) {
            return (User) users.get(0);
        } else {
            return null;
        }
    }
}
