$(document).ready(function () {
    setInterval(uploadMessages, 100);
    sendMessage();
})

/**
 * This function returns application pass. If I'll delete it, when user will login
 * from another computer, and he'll tries load cities, he'll go to
 * http://localhost:8080/updateCities (In case with another computer in my net,
 * it was http://192.168.1.141/updateCities)
 * @returns {string}
 */
function baseUrl() {
    var href = window.location.href.split('/');
    return href[0] + '//' + href[2] + '/';
}

function uploadMessages() {
    window.console.log(new Date());
    window.console.log(baseUrl() + "getMessages");
    $.post(baseUrl() + "getMessages",
        {
            conversationId: $("#conversationId").val()
        },
        function (data) {
            $("#messagesBlock").empty()
            for (var i = 0; i < data.length; i++) {
                $("#messagesBlock").append(data[i].author.firstName + ': ' + data[i].message + ' (' + data[i].createdAtStr + ')' + '<br/>');
            }
        }
    );
}

function sendMessage() {
    $("#submitMessage").click(function() {
        $.post(baseUrl() + "sendAjaxMessage",
            {
                receiverId: $("#receiverId").val(),
                message: $("#message").val()
            },
            function(data) {
                if (data == 'ignored') {
                    alert("You can't send messages to this user, he ignored you!")
                }
                $("#message").val("");
            }
        );
    })
}