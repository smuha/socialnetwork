
/**
 * This function returns application pass. If I'll delete it, when user will login
 * from another computer, and he'll tries load cities, he'll go to
 * http://localhost:8080/updateCities (In case with another computer in my net,
 * it was http://192.168.1.141/updateCities)
 * @returns {string}
 */
function baseUrl() {
    var href = window.location.href.split('/');
    return href[0]+'//'+href[2]+'/';
}

$(document).ready(function() {
    $('#uploadAvatarForm').ajaxForm(function(data) {
        if (data.state == "success") {
            if ($("#imageAvatar").attr('src') == '../../resourses/avatars/noPhoto.png') {
                location.reload();
            } else {
                $("#imageAvatar").animate({ opacity: "0" }, 0);
                $("#imageAvatar").attr('src', $("#imageAvatar").attr('src').replace(data.oldPathToAvatar, data.newPathToAvatar));
                $("#imageAvatar").animate({ opacity: "1" }, 600);

                //For IE
                $("#inputFile").replaceWith($("#inputFile").clone(true));
                //For other browsers
                $("#inputFile").val("");
            }
        }

//        $("#state").text(data.message);
//        $("#state").css("display","block");
//        setTimeout(' $("#state").css("display","none"); ', 3000)
    });
});

