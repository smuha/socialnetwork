$( document ).ready( function() {
    if ($( "#selectCountryId option:selected" ).val() == 0) {
        $("#divSelectCity").css("display","none");
    }
    changeCountryHandler();
})

function changeCountryHandler() {
    $("#selectCountryId").change(function() {
        $.post(baseUrl() + "updateCities",
            {
                countryId: $("#selectCountryId").val()
            },
            function(data) {
                if ( $("#selectCountryId").val() == 0) {
                    $("#divSelectCity").css("display","none");
                } else {
                    var select = $('#selectCityId');
                    select.empty();
                    select.append('<option value="0">-None selected-</option>');
                    for (var i = 0; i < data.length; i++) {
                        select.append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                    }
                    $("#divSelectCity").css("display","table-row");
                }
            }
        );
    })
}


/**
 * This function returns application pass. If I'll delete it, when user will login
 * from another computer, and he'll tries load cities, he'll go to
 * http://localhost:8080/updateCities (In case with another computer in my net,
 * it was http://192.168.1.141/updateCities)
 * @returns {string}
 */
function baseUrl() {
    var href = window.location.href.split('/');
    return href[0]+'//'+href[2]+'/';
}