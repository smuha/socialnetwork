$.validator.addMethod('customphone', function (value, element)
{
    return this.optional(element) || /^\d{3}\d{3}\d{4}$/.test(value);
}, "Please enter a valid phone number in this format: 0########");


$.validator.addMethod("lettersonly", function(value, element)
{
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
}, "Letters and spaces only please");

$(function () {

    $('#registrationForm').validate({
        rules: {
            firstName: {
                required: true,

                lettersonly:true
            },
            lastName:{
                required: true,

                lettersonly:true
            },
            email: {
                email: true,
                required: true
            },
            phone: {
                customphone: true,
                required: true
            },
            password: "required",
            repeatPass: {
                required: true,
                equalTo: "#password"
            }

        },
        messages: {
            firstName: {
                required: "&nbsp;&nbsp;Please enter your name",

                lettersonly:"&nbsp;&nbsp;First name accept only letters"

            },
            lastName:{
                required: "&nbsp;&nbsp;Please enter your name",

                lettersonly:"&nbsp;&nbsp;Last name accept only letters"
            },
            email: {
                email: "&nbsp;&nbsp;Please enter your email.",
                required: "&nbsp;&nbsp;Please enter the correct email."
            },
            phone: "&nbsp;&nbsp;Please enter phone number in this format: '0########'",
            password: "&nbsp;&nbsp;Please enter your password",
            repeatPass: {
                equalTo: "&nbsp;&nbsp;Password must be equal",
                required: "&nbsp;&nbsp;Please confirm your password"
            }
        },
        errorContainer: $('#errorContainer'),
        errorLabelContainer: $('#errorContainer ul'),
        wrapper: 'li'
    });

});
