<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="utf-8"/>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title>Conversations</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <link rel="stylesheet" href="<c:url value="/resourses/css/style.css" />">
</head>

<body>

<div class="wrapper">

    <%@ include file="sidebars/header.jsp" %>

    <div class="middle">

        <div class="container">
            <main class="content">
                <div class="contentChapter">
                    Conversations
                </div>
                <c:forEach items="${conversations}" var="conversation">
                    <div>
                        Dialog with
                        <a class="linkToUser" href="/chat?conversationId=${conversation.id}">
                            <c:choose>
                                <c:when test="${conversation.userOne.id == loggedUser.id}">
                                    ${conversation.userTwo.firstName}
                                    ${conversation.userTwo.lastName}
                                </c:when>
                                <c:otherwise>
                                    ${conversation.userOne.firstName}
                                    ${conversation.userOne.lastName}
                                </c:otherwise>
                            </c:choose>
                            <c:if test="${conversation.amountUnreadMessages != 0}">
                                (${conversation.amountUnreadMessages})
                            </c:if>
                        </a>
                    </div>
                </c:forEach>

            </main>
            <!-- .content -->
        </div>
        <!-- .container-->

        <aside class="left-sidebar">
            <%@ include file="sidebars/leftSidebar.jsp" %>
        </aside>
        <!-- .left-sidebar -->

    </div>
    <!-- .middle-->

</div>
<!-- .wrapper -->

<%@ include file="sidebars/footer.jsp" %>

</body>
</html>
