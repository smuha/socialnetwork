<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8" />
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title>Dialog with  ${messageReceiver.firstName}</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link rel="stylesheet" href="<c:url value="/resourses/css/style.css" />">
    <script src="/resourses/js/jquery-2.1.0.min.js"></script>
    <script src="/resourses/js/chatSync.js"></script>
</head>

<body>

<div class="wrapper">

    <%@ include file="sidebars/header.jsp"%>

    <div class="middle">

        <div class="container">
            <main class="content">

                <div class="contentChapter">
                    Dialog with
                    <a class="linkToUser" class="linkToUser" href="/profile?id=${messageReceiver.id}">
                     ${messageReceiver.firstName} ${messageReceiver.lastName}
                    </a>:
                </div>

                <br/>
                <div id="messagesBlock">
                    <c:forEach items="${messages}" var="message">
                        <div>
                                ${message.author.firstName}: ${message.message} (${message.createdAt})
                        </div>
                    </c:forEach>
                </div>

                <br/>
                <div id="sendMessageForm">
                    <div>
                        <textarea id="message" name="message" rows="3" cols="50"></textarea>
                    </div>
                    <input id="receiverId" type="hidden" name="receiverId" value="${messageReceiver.id}">

                    <!-- conversationId I use, because I'll get value from it to send ajax
                     request to the controller "getMessages. BE CAREFUL, IF MESSAGES WILL BE NO,
                     YOU WILL HAVE EXAPTION" -->
                    <input type="hidden" id="conversationId" value="${messages[0].conversation.id}">

                    <input id="submitMessage" type="submit" value="Send message" />
                </div>

                <%--<form action="sendMessage" method="post">--%>
                    <%--<div>--%>
                        <%--<textarea name="message" rows="3" cols="50"></textarea>--%>
                    <%--</div>--%>
                    <%--<input type="hidden" name="receiverId" value="${messageReceiver.id}">--%>

                    <%--<!-- conversationId I use, because I'll get value from it to send ajax--%>
                     <%--request to the controller "getMessages. BE CAREFUL, IF MESSAGES WILL BE NO,--%>
                     <%--YOU WILL HAVE EXAPTION" -->--%>
                    <%--<input type="hidden" id="conversationId" value="${messages[0].conversation.id}">--%>

                    <%--<input type="submit" value="Send message" />--%>
                <%--</form>--%>

            </main><!-- .content -->
        </div><!-- .container-->

        <aside class="left-sidebar">
            <%@ include file="sidebars/leftSidebar.jsp"%>
        </aside><!-- .left-sidebar -->

    </div><!-- .middle-->

</div><!-- .wrapper -->

<%@ include file="sidebars/footer.jsp"%>

</body>
</html>

