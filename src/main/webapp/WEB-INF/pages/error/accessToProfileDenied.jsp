<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8" />
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link rel="stylesheet" href="<c:url value="/resourses/css/style.css" />">
</head>

<body>

<div class="wrapper">

    <%@ include file="../sidebars/header.jsp"%>

    <div class="middle">

        <div class="container">
            <main class="content">

                <div style="text-align: center;">
                    <img src="../../../resourses/images/access_denied.png">
                    <p style="  font-size: 20px;">${user.firstName} ${user.lastName} has restricted access to his profile!</p>
                </div>

            </main><!-- .content -->
        </div><!-- .container-->

        <aside class="left-sidebar">
            <%@ include file="../sidebars/leftSidebar.jsp"%>
        </aside><!-- .left-sidebar -->

    </div><!-- .middle-->

</div><!-- .wrapper -->

<%@ include file="../sidebars/footer.jsp"%>

</body>
</html>
