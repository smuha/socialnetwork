<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="leftMenuLinks">
    <div>
        <a href="/profile?id=${loggedUser.id}">My profile</a>
    </div>
    <div>
        <a href="/friends?id=${loggedUser.id}">Friends</a><span id="amountOfNewFriends" style="display: none"></span>
    </div>
    <div>
        <a href="conversations">Conversations</a>
    </div>
    <div>
        <a href="ignoreList">Ignore list</a>
    </div>
    <div>
        <a href="edit?id=${loggedUser.id}">Edit profile</a>
    </div>
</div>
