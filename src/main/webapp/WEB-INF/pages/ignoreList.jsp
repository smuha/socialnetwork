<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8"/>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title>Ignore list</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <link rel="stylesheet" href="<c:url value="/resourses/css/style.css" />">
</head>

<body>

<div class="wrapper">

    <%@ include file="sidebars/header.jsp" %>

    <div class="middle">

        <div class="container">
            <main class="content">
                <p></p>

                <div class="contentChapter">
                    Ignore list
                </div>
                <br/>

                <table style="margin-left: auto; margin-right: auto; width: 500px;">
                    <tr class="subtitle">
                        <td style="height: 28px;">
                            All users
                        </td>
                        <td style="height: 28px;">
                            Ignored users
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <table valign="middle">
                                <c:forEach items="${people}" var="person">
                                    <c:if test="${person.id != loggedUser.id}">
                                        <tr style="padding-bottom: 2em;">
                                            <td align="center" valign="middle" style="height: 35px; width: 36px;" >
                                                <div style="min-height: 30px; min-width: 30px;">
                                                    <img class="thumbAvatar" onerror="this.src='../../resourses/avatars/noPhoto.png';" src="../../resourses/avatars/${person.pathToCurrentAvatar}"/>
                                                </div>
                                            </td>
                                            <td style="width: 180px; vertical-align: top;">
                                                <div class="linkToUser">
                                                    <a class="linkToUser" href="/profile?id=${person.id}">${person.firstName} ${person.lastName}</a>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <a href="/ignoreUser?userId=${person.id}">
                                                    <img style="width: 17px;" src="../../resourses/images/add.png">
                                                </a>
                                            </td>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </table>
                        </td>

                        <td>
                            <table valign="middle">
                                <c:forEach items="${ignoredUsers}" var="ignoredUser">
                                    <tr style="padding-bottom: 1em;">
                                        <td align="center" valign="middle">
                                            <div style="min-height: 30px; min-width: 30px; height: 35px; width: 36px;">
                                                <img class="thumbAvatar" onerror="this.src='../../resourses/avatars/noPhoto.png';" src="../../resourses/avatars/${ignoredUser.pathToCurrentAvatar}"/>
                                            </div>
                                        </td>
                                        <td style="width: 180px; vertical-align: top;">
                                            <div class="linkToUser">
                                                <a class="linkToUser" href="/profile?id=${ignoredUser.id}">${ignoredUser.firstName} ${ignoredUser.lastName}</a>
                                            </div>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <a href="/removeFromIgnoreList?id=${ignoredUser.id}">
                                                <img style="width: 17px;" src="../../resourses/images/remove.png">
                                            </a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </td>
                    </tr>
                </table>
            </main>
            <!-- .content -->
        </div>
        <!-- .container-->

        <aside class="left-sidebar">
            <%@ include file="sidebars/leftSidebar.jsp" %>
        </aside>
        <!-- .left-sidebar -->

    </div>
    <!-- .middle-->

</div>
<!-- .wrapper -->

<%@ include file="sidebars/footer.jsp" %>

</body>
</html>
