<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<header class="header">
    <div class="logoTitle">
        Simple social network
    </div>

    <c:choose>
        <c:when test="${entered == true && entered != null}">
            <%@ include file="../menus/topMenu.jsp"%>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
</header><!-- .header-->