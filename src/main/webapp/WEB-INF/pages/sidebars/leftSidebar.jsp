<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${entered}">
        <%@ include file="../menus/leftMenu.jsp"%>
    </c:when>
    <c:otherwise>
        <%@ include file="../forms/loginForm.jsp"%>
    </c:otherwise>
</c:choose>
