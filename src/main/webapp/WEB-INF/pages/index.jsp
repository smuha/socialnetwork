<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8" />
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title>GeekHub</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link rel="stylesheet" href="<c:url value="/resourses/css/style.css" />">
    <script src="<c:url value="/resourses/js/jquery-1.5.2.min.js" />"></script>
    <script src="<c:url value="/resourses/js/jquery.validate.min.js" />"></script>
    <script src="<c:url value="/resourses/js/validationRegistrationForm.js" />"></script>
</head>

<body>

<div class="wrapper">

    <%@ include file="sidebars/header.jsp"%>

    <div class="middle">

        <div class="container">
            <main class="content">
                <%@ include file="registration.jsp"%>
            </main><!-- .content -->
        </div><!-- .container-->

        <aside class="left-sidebar">
            <%@ include file="login.jsp"%>
        </aside><!-- .left-sidebar -->

    </div><!-- .middle-->

</div><!-- .wrapper -->

<%@ include file="sidebars/footer.jsp"%>

</body>
</html>
