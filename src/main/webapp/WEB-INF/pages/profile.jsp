<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <meta charset="utf-8"/>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <script src="/resourses/js/jquery-2.1.0.min.js"></script>
    <script src="/resourses/js/jquery.form.js"></script>
    <title>${user.firstName} ${user.lastName}</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <link rel="stylesheet" href="<c:url value="/resourses/css/style.css" />">
</head>

<body>

<div class="wrapper">

    <%@ include file="sidebars/header.jsp" %>

    <div class="middle">

        <div class="container">
            <main class="content">
                <div id="avatar">
                    <img onerror="this.src='../../resourses/avatars/noPhoto.png';"
                         src="../../resourses/avatars/${user.pathToCurrentAvatar}"
                         style="max-width: 250px; max-height: 400px;"/>
                </div>

                <div>
                    <div id="summary">
                        <div id="profileName">
                            ${user.firstName} ${user.lastName}
                        </div>
                        <div>
                            E-mail: ${user.email}
                        </div>
                        <div>
                            Phone: ${user.phone}
                        </div>
                        <c:if test="${user.birthday != null}">
                            <div>
                                Birthday: ${user.birthday}
                            </div>
                        </c:if>
                        <c:if test="${user.country != null}">
                            <div>
                                Country: ${user.country.name}
                            </div>
                        </c:if>
                        <c:if test="${user.city != null}">
                            <div>
                                City: ${user.city.name}
                            </div>
                        </c:if>
                    </div>
                </div>

                <c:choose>
                    <c:when test="${loggedUser.id == user.id}">

                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${isFriendToCurrentUser == false}">
                                <form action="/addFriend" method="post">
                                    <input type="hidden" name="id" value="${user.id}"/>
                                    <input type="submit" value="Add to friend"/>
                                </form>
                            </c:when>
                            <c:otherwise>
                                <form action="/removeFriend" method="post">
                                    <input type="hidden" name="id" value="${user.id}"/>
                                    <input type="submit" value="Remove from friend"/>
                                </form>
                            </c:otherwise>
                        </c:choose>

                        <form action="/sendMessage" method="get">
                            <input type="hidden" name="receiverId" value="${user.id}"/>
                            <input type="submit" value="Send message"/>
                        </form>

                        <a href="/friends?id=${user.id}">Friends</a>
                    </c:otherwise>
                </c:choose>

            </main>
            <!-- .content -->
            <div id="wall" style="margin-top: 23%; margin-left: 25%; margin-bottom: 25%;"><!---#wall -->
                <%@include file="forms/wallPostForm.jsp" %>
                <div id="postWall">
                    <%@include file="forms/postWallSystem.jsp"%>
                </div>
            </div>
        </div>
        <!-- .container-->

        <aside class="left-sidebar">
            <%@ include file="sidebars/leftSidebar.jsp" %>
        </aside>
        <!-- .left-sidebar -->

    </div>
    <!-- .middle-->

</div>
<!-- .wrapper -->

<%@ include file="sidebars/footer.jsp" %>

</body>
</html>