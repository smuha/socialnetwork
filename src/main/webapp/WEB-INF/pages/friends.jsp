<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8"/>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title>Friends</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <link rel="stylesheet" href="<c:url value="/resourses/css/style.css" />">
</head>

<body>

<div class="wrapper">

    <%@ include file="sidebars/header.jsp" %>

    <div class="middle">

        <div class="container">
            <main class="content">
                <c:choose>
                    <c:when test="${loggedUser.id == user.id}">
                        <div class="contentChapter">
                            Friends
                        </div>
                        <br/>
                    </c:when>
                    <c:otherwise>
                        <div class="contentChapter">
                            Friends of
                            <a class="linkToUser" href="/profile?id=${user.id}">
                                    ${user.firstName} ${user.lastName}
                            </a>: <br/><br/>
                        </div>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${friends == null}">
                        No friends yet..
                    </c:when>
                    <c:otherwise>
                        <table style="margin-left: auto; margin-right: auto; width: 400px;">
                            <c:forEach items="${friends}" var="friend">
                                <tr>
                                    <td style="height: 35px;">
                                        <div style="min-height: 30px; min-width: 30px;">
                                            <img class="thumbAvatar"
                                                 onerror="this.src='../../resourses/avatars/noPhoto.png';"
                                                 src="../../resourses/avatars/${friend.pathToCurrentAvatar}"/>
                                        </div>
                                    </td>
                                    <td class="linkToUser" style="width: 300px; vertical-align: top;">
                                        <a class="linkToUser"
                                           href="/profile?id=${friend.id}"> ${friend.firstName} ${friend.lastName}</a>
                                    </td>
                                    <td style="width: 62px;">
                                        <c:if test="${loggedUser.id == user.id}">
                                            <form action="/removeFriend" method="post">
                                                <input type="hidden" name="id" value="${friend.id}"/>
                                                <input type="submit" value="Remove"/>
                                            </form>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </c:otherwise>
                </c:choose>
            </main>
            <!-- .content -->
        </div>
        <!-- .container-->

        <aside class="left-sidebar">
            <%@ include file="sidebars/leftSidebar.jsp" %>
        </aside>
        <!-- .left-sidebar -->

    </div>
    <!-- .middle-->

</div>
<!-- .wrapper -->

<%@ include file="sidebars/footer.jsp" %>

</body>
</html>
