
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<ul id="post">
    <c:forEach items="${posts}" var="post">
        ${post.createdBy.firstName} ${post.createdBy.lastName}: ${post.title}<br/>

        <form action="/createComment" method="post">

            <textarea name="comment" id="postComment" cols="10" rows="1"></textarea>

            <input name="user" type="hidden" value="${loggedUser.id}">
            <input name="post" type="hidden" value="${post.id}">
            <input type="submit" value="Comment">
        </form>

        <li id="comment">
            <c:forEach items="${postComments}" var="postComment">
                Comment: ${postComment.comment}
            </c:forEach>
        </li>

    </c:forEach>
</ul>
