<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="<c:url value="/resourses/js/jquery-1.5.2.min.js" />"></script>
<script src="<c:url value="/resourses/js/jquery.validate.min.js" />"></script>
<script src="<c:url value="/resourses/js/validationRegistrationForm.js" />"></script>

<div style="width: 50%; margin: 0 auto;">
    <form id="registrationForm" action="/register" method="post">
        <table id="tableRegistrationForm">
            <tr>
                <td style="vertical-align: middle;">
                    <label for="firstName">First name:</label>
                </td>
                <td>
                    <input id="firstName" type="text" name = "firstName" placeholder="First Name" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="lastName">Last Name:</label>
                </td>
                <td>
                    <input id="lastName" type="text" name = "lastName" placeholder="Last Name"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="email">Email:</label>
                </td>
                <td>
                    <input  id="email" type="email" name = "email" placeholder="E-mail"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="phone">Phone:</label>
                </td>
                <td>
                    <input  id="phone" type="text" name = "phone" placeholder="Phone"/></p>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="password">Password:</label>
                </td>
                <td>
                    <input  id="password" type="password" name = "password" placeholder="password"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="repeatPass">Confirm password:</label>
                </td>
                <td>
                    <input id="repeatPass" type="password" name = "repeatPass" placeholder="password"/>
                </td>
            </tr>

            <div>
                <tr>
                    <td colspan="2" style="height: 35; vertical-align: bottom;">
                        <input style="width: 300px" type="submit" value="Register"/>
                    </td>
                </tr>
            </div>
            <br>
        </table>
    </form>
</div>

<div id="errorContainer" style="width: 450px; margin: 10 auto;">
    <p>&nbsp;Please correct the following errors and try again:</p>
    <ul />
</div>