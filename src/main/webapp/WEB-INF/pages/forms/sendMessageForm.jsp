<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form action="sendMessage" method="post">
    <h2>
        Send message to ${messageReceiver.firstName} ${messageReceiver.lastName}
    </h2>
    <div>
        Message body: <br/>
        <textarea name="message" rows="4" cols="50" autofocus="autofocus"></textarea>
    </div>
    <input type="hidden" name="receiverId" value="${messageReceiver.id}">
    <input type="submit" value="Send message" />
</form>
