<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form id="leftLoginFrom" action="login" method="post">
    <div>
        <input id="email" type="email" name="email" placeholder="E-mail" required="required">
    </div>
    <div>
        <input id="password" type="password" name="password" placeholder="Password" required="required">
    </div>
    <div>
        <input id="logSubmit" type="submit" value="Login">
    </div>
</form>
