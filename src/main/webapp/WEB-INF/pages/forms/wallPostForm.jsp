<%--
  Created by IntelliJ IDEA.
  User: Виктор
  Date: 15.04.14
  Time: 20:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script src="/resourses/js/sendPost.js"></script>
<form  method="post" id="createPost" action="/createPost">
    <textarea name="title" id="title" cols="50" rows="4" placeholder="What's new?"
              style="width:250px;height:100px;font-family:cursive;border:solid 12px #6DB72C; border-radius: 20px;"></textarea>
    <br>
    <input name="createdBy" id="createdBy" value="${loggedUser.id}" type="hidden">
    <input name="attachedTo" id="attachedTo" type="hidden" value="${user.id}">
    <input type="button" value="Share" id="sendPostWall" onclick="sendPost();">
</form>
