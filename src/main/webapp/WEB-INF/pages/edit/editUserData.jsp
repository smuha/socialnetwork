<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8" />
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title>Edit</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <script src="/resourses/js/jquery-2.1.0.min.js"></script>
    <script src="/resourses/js/editUserData.js"></script>
    <link rel="stylesheet" href="<c:url value="/resourses/css/style.css" />">
</head>

<body>

<div class="wrapper">

    <%@ include file="../sidebars/header.jsp"%>

    <div class="middle">

        <div class="container">
            <main class="content">
                <div class="contentChapter">
                    Edit profile
                </div>

                <br/>
                <table style="margin-left: auto; margin-right: auto; width: 300px;">
                <form action="update" method="post">
                    <tr>
                        <td class="editLabel" style="width: 130px; height: 25px;">First Name</td>
                        <td><input type="text" name = "firstName" value="${loggedUser.firstName}"/></td>
                    </tr>
                    <tr>
                        <td class="editLabel">Last Name</td>
                        <td><input type="text" name = "lastName" value="${loggedUser.lastName}"/></td>
                    </tr>
                    <tr>
                        <td class="editLabel">Middle Name</td>
                        <td><input type="text" name = "middleName" value="${loggedUser.middleName}" /></td>
                    </tr>
                    <tr>
                        <td class="editLabel">Phone</td>
                        <td><input type="text" name = "phone" value="${loggedUser.phone}"/></td>
                    </tr>
                    <tr>
                        <td class="editLabel">Birthday</td>
                        <td><input type="date" name = "birthday" value="${loggedUser.birthday}"/></td>
                    </tr>
                    <tr>
                        <td class="editLabel">Country</td>
                        <td>
                            <select id="selectCountryId" name='countryId'>
                                <option value="0">-None selected-</option>
                                <c:forEach items="${countries}" var="country">
                                    <c:choose>
                                        <c:when test="${country.id == loggedUser.country.id}">
                                            <option value="${country.id}" selected>${country.name}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${country.id}">${country.name}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr id="divSelectCity" style="display: table-row">
                        <td class="editLabel">City</td>
                        <td>
                            <select id="selectCityId" name='cityId'>
                                <option value="0">-None selected-</option>
                                <c:forEach items="${cities}" var="city">
                                    <c:choose>
                                        <c:when test="${city.id == loggedUser.city.id}">
                                            <option value="${city.id}" selected>${city.name}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${city.id}">${city.name}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 35px; vertical-align: middle;">
                            <input type="hidden" name = "id" value="${loggedUser.id}" style="width: 150px;"/>
                            <input style="width: 288" type="submit" value="Update" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center; height: 30px">
                            <a href="uploadAvatar">Change avatar</a>
                        </td>
                    </tr>
                </form>
                </table>

            </main><!-- .content -->
        </div><!-- .container-->

        <aside class="left-sidebar">
            <%@ include file="../sidebars/leftSidebar.jsp"%>
        </aside><!-- .left-sidebar -->

    </div><!-- .middle-->

</div><!-- .wrapper -->

<%@ include file="../sidebars/footer.jsp"%>

</body>
</html>