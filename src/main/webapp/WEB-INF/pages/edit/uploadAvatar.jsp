<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8"/>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title>Upload avatar</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <script src="/resourses/js/jquery-2.1.0.min.js"></script>
    <script src="/resourses/js/jquery.form.js"></script>
    <script src="/resourses/js/uploadAvatar.js"></script>
    <link rel="stylesheet" href="<c:url value="/resourses/css/style.css" />">
</head>

<body>

<div class="wrapper">

    <%@ include file="../sidebars/header.jsp" %>

    <div class="middle">

        <div class="container">
            <main class="content">

                <div>
                    Change current avatar, upload new! (Max size - 3MB)
                </div>

                <div id="state" style="display: none; background-color: green; padding-top: 10px"></div>

                <br/>
                <div>
                    <img id="imageAvatar" onerror="this.src='../../resourses/avatars/noPhoto.png';" src="../../resourses/avatars/${loggedUser.pathToCurrentAvatar}" style="min-height: 30px; max-width: 400px; max-height: 400"/>
                </div>

                <br/>
                <form id="uploadAvatarForm" method="POST" enctype="multipart/form-data" action="/uploadAvatar">
                    <input id="inputFile" type="file" name="file"/><br/>
                    <input type="submit" value="Upload">
                </form>

                <%--<div id="uploadForm">--%>
                    <%--<input id="inputFile" type="file" name="file"/><br/>--%>
                    <%--<input id="submit" type="submit" value="Upload">--%>
                <%--</div>--%>

            </main>
            <!-- .content -->
        </div>
        <!-- .container-->

        <aside class="left-sidebar">
            <%@ include file="../sidebars/leftSidebar.jsp" %>
        </aside>
        <!-- .left-sidebar -->

    </div>
    <!-- .middle-->

</div>
<!-- .wrapper -->

<%@ include file="../sidebars/footer.jsp" %>

</body>
</html>

