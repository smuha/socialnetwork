<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8"/>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title>People</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <link rel="stylesheet" href="<c:url value="/resourses/css/style.css" />">
</head>

<body>

<div class="wrapper">

    <%@ include file="sidebars/header.jsp" %>

    <div class="middle">

        <div class="container">
            <main class="content">

                <div class="contentChapter">
                    People
                </div>

                <br/>
                <table style="margin-left: auto; margin-right: auto; width: 400px;">
                    <c:forEach items="${people}" var="person">
                        <tr>
                            <td align="center" valign="middle" style="height: 35px;">
                                <div style="min-height: 30px; min-width: 30px;">
                                    <img class="thumbAvatar" onerror="this.src='../../resourses/avatars/noPhoto.png';" src="../../resourses/avatars/${person.pathToCurrentAvatar}"/>
                                </div>
                            </td>
                            <td style="width: 250px;  vertical-align: top;">
                                <div class="linkToUser">
                                    <a class="linkToUser" href="/profile?id=${person.id}">${person.firstName} ${person.lastName}</a>
                                </div>
                            </td>
                            <td style="height: 26px; vertical-align: middle;" align="center" valign="middle">
                                <c:set var="switch" scope="page" value="false"/>
                                <c:forEach items="${friends}" var="friend">
                                    <c:if test="${friend.id == person.id}">
                                        <c:set var="switch" scope="page" value="true"/>
                                    </c:if>
                                </c:forEach>
                                <c:forEach items="${ignoredIdUsers}" var="ignoredIdUser">
                                    <c:if test="${ignoredIdUser.id == person.id}">
                                        <c:set var="switch" scope="page" value="true"/>
                                    </c:if>
                                </c:forEach>

                                <c:if test="${!switch && person.id != loggedUser.id}">
                                    <form action="/addFriend" method="post">
                                        <input type="hidden" name="id" value="${person.id}"/>
                                        <input type="submit" value="Add to friend"/>
                                    </form>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                </table>

            </main>
            <!-- .content -->
        </div>
        <!-- .container-->

        <aside class="left-sidebar">
            <%@ include file="sidebars/leftSidebar.jsp" %>
        </aside>
        <!-- .left-sidebar -->

    </div>
    <!-- .middle-->

</div>
<!-- .wrapper -->

<%@ include file="sidebars/footer.jsp" %>

</body>
</html>
